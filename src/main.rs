extern crate serde;
extern crate serde_json;

use std::env;

#[derive(Debug)]
struct Vector {
    lat: f32,
    lon: f32,
    city: String,
}

struct VectorDB {
    vectors: Vec<Vector>,
}

impl VectorDB {
    fn new() -> VectorDB {
        VectorDB { vectors: Vec::new() }
    }

    // Add a new vector to the database. Adjust parameters as needed for your application.
    fn add_vector(&mut self, lat: f32, lon: f32, city: &str) {
        let vector = Vector {
            lat,
            lon,
            city: city.to_string(),
        };
        self.vectors.push(vector);
    }

    fn haversine_distance(a: &Vector, b: &Vector) -> f64 {
        let r = 6371e3; // Earth's radius in meters
        let lat1 = a.lat as f64;
        let lon1 = a.lon as f64;
        let lat2 = b.lat as f64;
        let lon2 = b.lon as f64;

        let delta_lat = (lat2 - lat1).to_radians();
        let delta_lon = (lon2 - lon1).to_radians();
        let a = (delta_lat / 2.0).sin().powi(2)
                + lat1.to_radians().cos() * lat2.to_radians().cos() * (delta_lon / 2.0).sin().powi(2);
        let c = 2.0 * a.sqrt().atan2((1.0 - a).sqrt());

        r * c // Distance in meters
    }

    fn find_closest(&self, query: &Vector) -> Option<&Vector> {
        self.vectors.iter().min_by(|&a, &b| {
            let distance_a = VectorDB::haversine_distance(&query, a);
            let distance_b = VectorDB::haversine_distance(&query, b);
            distance_a.partial_cmp(&distance_b).unwrap_or(std::cmp::Ordering::Equal)
        })
    }

    fn find_city(&self, city_name: &str) -> Vec<&Vector> {
        self.vectors.iter().filter(|&vector| vector.city == city_name).collect()
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    // Expecting the program to be called with three arguments: latitude, longitude, and city name.
    // Example usage: cargo run -- 0.5733 -2.0417 "San Diego"
    if args.len() < 4 {
        eprintln!("Usage: cargo run -- <latitude> <longitude> \"<city name>\"");
        return;
    }

    let lat = args[1].parse::<f32>().expect("Latitude must be a valid number");
    let lon = args[2].parse::<f32>().expect("Longitude must be a valid number");
    let city = &args[3];

    let mut db = VectorDB::new();
    db.add_vector(0.7842, 2.0413, "San Diego");
    db.add_vector(-0.2281, 1.1684, "San Jose");
    db.add_vector(0.6190, 2.3812, "San Jose");
    db.add_vector(0.7709, 2.1653, "New York");
    db.add_vector(-1.0876, -1.9070, "Phoenix");
    db.add_vector(0.2653, 2.2611, "Los Angeles");
    db.add_vector(-0.3663, 0.7592, "Chicago");
    db.add_vector(0.5355, -1.7147, "Phoenix");
    db.add_vector(0.3779, 2.4551, "San Diego");
    db.add_vector(1.3530, 0.5606, "Dallas");
    db.add_vector(0.5600, -2.0427, "San Diego");
    db.add_vector(0.5612, -2.0405, "San Diego");
    db.add_vector(0.6479, -2.1293, "San Jose");
    db.add_vector(0.6490, -2.1311, "San Jose");
    db.add_vector(0.5803, -1.9635, "Phoenix");
    db.add_vector(0.5811, -1.9612, "Phoenix");
    db.add_vector(0.7108, -1.2915, "New York");
    db.add_vector(0.5963, -2.0728, "Los Angeles");
    db.add_vector(0.7310, -1.5124, "Chicago");
    db.add_vector(0.5263, -1.6733, "Houston");
    db.add_vector(0.6991, -1.3487, "Philadelphia");
    db.add_vector(0.5105, -1.8131, "San Antonio");

    println!("Results:");
    let query_vector = Vector { lat, lon, city: city.to_string() };
    println!("Closest vector to ({}, {}):", lat, lon);
    if let Some(closest) = db.find_closest(&query_vector) {
        println!("{:?}", closest);
    } else {
        println!("No closest vector found.");
    }
    println!(" ");
    let city_vectors = db.find_city(city);
    println!("Vectors in {}:", city);
    for vector in city_vectors {
        println!("{:?}", vector);
    }
}