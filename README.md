# Mini Project 7: Data Processing in Vector Database
## Project Description
This project requires us to implement a Vector Database and do data processing with this database. For this specific project, I implement a vector database that takes in vector containing latitude, longitude, and city names and return results after some filters and aggregation.

## Vector Database Structure
The Vector Database has following three fields:
```rust
struct Vector {
    lat: f32, // Latitude
    lon: f32, // Longitude
    city: String, // City name
}
```

## Function and database usage
The program takes in three arguments for criterion and could be ran using following command:
```bash
cargo run -- 0.630 -2.057 "San Diego"
```
where each argument correspondes to the fields in the vector.

The database contains 4 functionalities:
1. **Creation of database**: database is created by using following code:
    ```rust
    fn new() -> VectorDB {
        VectorDB { vectors: Vec::new() }
    }
    ```
2. **Adding vector to a database**: vectors are allowed to add to the database corresponding to the vector fields defined above with the function:
    ```rust
    fn add_vector(&mut self, lat: f32, lon: f32, city: &str) {
        let vector = Vector {
                lat,
                lon,
                city: city.to_string(),
            };
            self.vectors.push(vector);
    }
    ```
   Example usage of adding vector could be performed in the main:
    ```rust
    db.add_vector(0.7842, 2.0413, "San Diego");
    ```
3. **Finding closest vectors**: The closness of the vectors are found using Harversine Distance, which takes in two pairs of latitude and longitude and calculate the distance between them. With the two functions below, we will calculate distance between given argument from users and each vector in the database and return the closest one:
    ```rust
    fn haversine_distance(a: &Vector, b: &Vector) -> f64 {
    }
    fn find_closest(&self, query: &Vector) -> Option<&Vector> {
        self.vectors.iter().min_by(|&a, &b| {
            let distance_a = VectorDB::haversine_distance(&query, a);
            let distance_b = VectorDB::haversine_distance(&query, b);
            distance_a.partial_cmp(&distance_b).unwrap_or(std::cmp::Ordering::Equal)
        })
    }
    ```
   To be more specific, it will take users' first two arguments and return the closest vector to the user given vectors.
4. **Filtering city**: Given the users' third argument, return the vectors containing same city name as user provided:
    ```rust
    fn find_city(&self, city_name: &str) -> Vec<&Vector> {
            self.vectors.iter().filter(|&vector| vector.city == city_name).collect()
        }
    ```

## Steps
1. run `cargo new <YOUR PROJECT NAME>` to create a new rust project.
2. Go to `main.rs`, start to write your implementation of your own Vector database. You could change the functionality and fields of vector with your own choice. Make sure to add the corresponding dependencies in `Cargo.toml`. Add `println` to display necessary output.
3. When you finish writing, make sure to run `cargo build` and  `cargo run -- arg1 arg2` to check if your database is working.

## Output display
![output](https://gitlab.com/HathawayLiu/mini_proj_7_hathaway_liu/-/wikis/uploads/9337117a9ad9c4edc688180fb97791ab/Screenshot_2024-03-23_at_2.04.12_PM.png)
## References
- [Medium Blog Post: Implementing a Vector DB](https://blog.devgenius.io/implementing-a-vector-database-in-rust-1272c89ca9f6)